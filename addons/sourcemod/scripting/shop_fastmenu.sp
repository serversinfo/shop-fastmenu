#include <shop>
#include <sdktools>
#pragma semicolon 1
#pragma newdecls required

bool bTagUsed[MAXPLAYERS];

public void OnPluginStart()
{
	RegConsoleCmd("sm_shopmenu", shopmenu);
	RegConsoleCmd("shopmenu", shopmenu);
	RegConsoleCmd("sh", shopmenu);
	HookEvent("round_start", eV_RoundStart, EventHookMode_PostNoCopy);
}

public void eV_RoundStart(Handle event, const char[] name, bool DB)
{
	for(int i=0; i<MAXPLAYERS; i++)
		bTagUsed[i] = false;
}

public Action shopmenu(int client, int args)
{
	if (client && GetClientTeam(client) == 3) {
		if (!args) {
			Main_Menu(client);
			ReplyToCommand(client, "Usage he - hegrenade, hs - healthshot (or hs 4), fb - flashbang, sm - smokegrenade, tag - tagrenade");
		} else {
			char cWeap[16];
			GetCmdArgString(cWeap, sizeof(cWeap));
			if (StrEqual(cWeap, "he"))
				SellItem(client, 7, "weapon_hegrenade");
			else if (StrEqual(cWeap, "hhss"))
				SellItem(client, 15, "weapon_healthshot");
			else if (StrEqual(cWeap, "fb"))
				SellItem(client, 4, "weapon_flashbang");
			else if (StrEqual(cWeap, "sm"))
				SellItem(client, 4, "weapon_smokegrenade");
			else if (StrEqual(cWeap, "tag")) {
				if (!bTagUsed[client]) {
					bTagUsed[client] = true;
					SellItem(client, 25, "weapon_tagrenade");
				} else PrintToChat(client, "Только одна граната за раунд");
			} else {
				char cHs[8];
				int len = BreakString(cWeap, cHs, sizeof(cHs));
				if (StrEqual(cHs, "hhss")) {
					char cNum[4];
					int next_len = BreakString(cWeap[len], cNum, sizeof(cNum));
					len += next_len;
					int iNum = StringToInt(cNum, sizeof(cNum));
					for(int i=0; i<iNum && i<8; i++)
						SellItem(client, 15, "weapon_healthshot");
				} else ReplyToCommand(client, "Usage he - hegrenade, hs - healthshot (or hs 4), fb - flashbang, sm - smokegrenade, tag - tagrenade");
			}
		}
	} else if (GetClientTeam(client) != 3) PrintToChat(client, "Только для КТ");
	return Plugin_Handled;
}

void Main_Menu(int client)
{
	Handle menu = CreateMenu(MainMenu);
	SetMenuTitle(menu, "Fast Shop Menu:\n $%i\n\n", Shop_GetClientCredits(client));
	AddMenuItem(menu, "1", "Пистолеты");
	AddMenuItem(menu, "2", "Тяжолое оружие");
	AddMenuItem(menu, "3", "Пистолеты пулеметы");
	AddMenuItem(menu, "4", "Штурмовые винтовки");
	AddMenuItem(menu, "5", "Гранаты");
	DisplayMenu(menu, client, 0);
}

public int MainMenu(Handle menu, MenuAction action, int client, int option) 
{
	if (action == MenuAction_End) {
		CloseHandle(menu);
		return;
	}
	if (action == MenuAction_Select) {
		char opti[4];
		GetMenuItem(menu, option, opti, 15);
		int opt = StringToInt(opti);
		switch (opt) {
			case 1 : PistolMenu(client);
			case 2 : SecondMenu(client);
			case 3 : ThirdMenu(client);
			case 4 : RiflesMenu(client);
			case 5 : GrenadesMenu(client);
		}
	} else return;
}

void PistolMenu(int client)
{
	Handle menu = CreateMenu(Pistol);
	SetMenuTitle(menu, "Fast Shop Menu:\n $%i\n\n", Shop_GetClientCredits(client));
	AddMenuItem(menu, "1", "elite [$2]");
	AddMenuItem(menu, "2", "p250 [$2]");
	AddMenuItem(menu, "3", "hkp2000 [$2]");
	AddMenuItem(menu, "4", "fiveseven [[$2]");
	AddMenuItem(menu, "5", "cz75a [$2]");
	AddMenuItem(menu, "6", "tec9 [$2]");
	AddMenuItem(menu, "7", "usp [$2]");
	AddMenuItem(menu, "8", "deagle [$2]");
	AddMenuItem(menu, "9", "revolver [$2]");
	DisplayMenu(menu, client, 0);
}

public int Pistol(Handle menu, MenuAction action, int client, int option) 
{
	if (action == MenuAction_End) {
		CloseHandle(menu);
		return;
	}
	if (action == MenuAction_Select) {
		char opti[4];
		GetMenuItem(menu, option, opti, 15);
		int opt = StringToInt(opti);
		switch (opt) {
			case 1 : SellItem(client, 2, "weapon_elite");
			case 2 : SellItem(client, 2, "weapon_p250");
			case 3 : SellItem(client, 2, "weapon_hkp2000");
			case 4 : SellItem(client, 2, "weapon_fiveseven");
			case 5 : SellItem(client, 2, "weapon_cz75a");
			case 6 : SellItem(client, 2, "weapon_tec9");
			case 7 : SellItem(client, 2, "weapon_usp_silencer");
			case 8 : SellItem(client, 2, "weapon_deagle");
			case 9 : SellItem(client, 2, "weapon_revolver");
		}
	} else return;
}

void SecondMenu(int client)
{
	Handle menu = CreateMenu(Second);
	SetMenuTitle(menu, "Fast Shop Menu:\n $%i\n\n", Shop_GetClientCredits(client));
	AddMenuItem(menu, "1", "xm1014 [$2]");
	AddMenuItem(menu, "2", "nova [$3]");
	AddMenuItem(menu, "3", "sawedoff [$2]");
	AddMenuItem(menu, "4", "mag7 [$2]");
	AddMenuItem(menu, "5", "m249 [$10]");
	AddMenuItem(menu, "6", "negev [$12]");
	DisplayMenu(menu, client, 0);
}

public int Second(Handle menu, MenuAction action, int client, int option) 
{
	if (action == MenuAction_End) {
		CloseHandle(menu);
		return;
	}
	if (action == MenuAction_Select) {
		char opti[4];
		GetMenuItem(menu, option, opti, 15);
		int opt = StringToInt(opti);
		switch (opt) {
			case 1 : SellItem(client, 2, "weapon_xm1014");
			case 2 : SellItem(client, 3, "weapon_nova");
			case 3 : SellItem(client, 2, "weapon_sawedoff");
			case 4 : SellItem(client, 2, "weapon_mag7");
			case 5 : SellItem(client, 10, "weapon_m249");
			case 6 : SellItem(client, 12, "weapon_negev");
		}
	} else return;
}

void ThirdMenu(int client)
{
	Handle menu = CreateMenu(Third);
	SetMenuTitle(menu, "Fast Shop Menu:\n $%i\n\n", Shop_GetClientCredits(client));
	AddMenuItem(menu, "1", "p90 [$6]");
	AddMenuItem(menu, "2", "ump45 [$3]");
	AddMenuItem(menu, "3", "mac10 [$3]");
	AddMenuItem(menu, "4", "mp7 [$3]");
	AddMenuItem(menu, "5", "mp9 [$3]");
	AddMenuItem(menu, "6", "bizon [$5]");
	DisplayMenu(menu, client, 0);
}

public int Third(Handle menu, MenuAction action, int client, int option) 
{
	if (action == MenuAction_End) {
		CloseHandle(menu);
		return;
	}
	if (action == MenuAction_Select) {
		char opti[4];
		GetMenuItem(menu, option, opti, 15);
		int opt = StringToInt(opti);
		switch (opt) {
			case 1 : SellItem(client, 6, "weapon_p90");
			case 2 : SellItem(client, 3, "weapon_ump45");
			case 3 : SellItem(client, 3, "weapon_mac10");
			case 4 : SellItem(client, 3, "weapon_mp7");
			case 5 : SellItem(client, 3, "weapon_mp9");
			case 6 : SellItem(client, 5, "weapon_bizon");
		}
	} else return;
}

void RiflesMenu(int client)
{
	Handle menu = CreateMenu(Rifles);
	SetMenuTitle(menu, "Fast Shop Menu:\n $%i\n\n", Shop_GetClientCredits(client));
	AddMenuItem(menu, "1", "ak47 [$6]");
	AddMenuItem(menu, "2", "m4a1 [$6]");
	AddMenuItem(menu, "3", "aug [$3]");
	AddMenuItem(menu, "4", "sg556 [$3]");
	AddMenuItem(menu, "5", "awp [$6]");
	AddMenuItem(menu, "6", "famas [$3]");
	AddMenuItem(menu, "7", "g3sg1 [$9]");
	AddMenuItem(menu, "8", "scar20 [$9]");
	DisplayMenu(menu, client, 0);
}

public int Rifles(Handle menu, MenuAction action, int client, int option) 
{
	if (action == MenuAction_End) {
		CloseHandle(menu);
		return;
	}
	if (action == MenuAction_Select) {
		char opti[4];
		GetMenuItem(menu, option, opti, 15);
		int opt = StringToInt(opti);
		switch (opt) {
			case 1 : SellItem(client, 6, "weapon_ak47");
			case 2 : SellItem(client, 6, "weapon_m4a1");
			case 3 : SellItem(client, 3, "weapon_aug");
			case 4 : SellItem(client, 3, "weapon_sg556");
			case 5 : SellItem(client, 6, "weapon_awp");
			case 6 : SellItem(client, 3, "weapon_famas");
			case 7 : SellItem(client, 9, "weapon_g3sg1");
			case 8 : SellItem(client, 9, "weapon_scar20");
		}
	} else return;
}

void GrenadesMenu(int client)
{
	Handle menu = CreateMenu(Grenades);
	SetMenuTitle(menu, "Fast Shop Гранаты:\n $%i\n\n", Shop_GetClientCredits(client));
	AddMenuItem(menu, "1", "Зажигательная граната [$5]");
	AddMenuItem(menu, "2", "TAG Grenade [$25]");
	AddMenuItem(menu, "3", "Световая граната [$4]");
	AddMenuItem(menu, "4", "Осколочная граната [$7]");
	AddMenuItem(menu, "5", "Дымовая граната [$4]");
	DisplayMenu(menu, client, 0);
}

public int Grenades(Handle menu, MenuAction action, int client, int option) 
{
	if (action == MenuAction_End) {
		CloseHandle(menu);
		return;
	}
	if (action == MenuAction_Select) {
		char opti[4];
		GetMenuItem(menu, option, opti, 15);
		int opt = StringToInt(opti);
		switch (opt) {
			case 1 : SellItem(client, 5, "weapon_incgrenade");
			case 2 : {
				if (!bTagUsed[client]) {
					bTagUsed[client] = true;
					SellItem(client, 25, "weapon_tagrenade");
				} else PrintToChat(client, "Только одна граната за раунд");
			}
			case 3 : SellItem(client, 4, "weapon_flashbang");
			case 4 : SellItem(client, 7, "weapon_hegrenade");
			case 5 : SellItem(client, 4, "weapon_smokegrenade");
			case 6 : SellItem(client, 15, "weapon_healthshot");
		}
		GrenadesMenu(client);
	} else return;
}

void SellItem(int client, int price, char[] weapon)
{
	if (IsPlayerAlive(client) && Shop_GetClientCredits(client) > price && GetClientTeam(client) == 3) {
		GivePlayerItem(client, weapon);
		Shop_TakeClientCredits(client, price, CREDITS_BY_NATIVE);
	}
	else if (GetClientTeam(client) != 3)
		PrintToChat(client, "Только для КТ");
	else if (Shop_GetClientCredits(client) <= price)
		PrintToChat(client, "Не хватает денег, нужно %i, а у тебя %i", price, Shop_GetClientCredits(client));
	else PrintToChat(client, "Вы должны быть живы.");
}